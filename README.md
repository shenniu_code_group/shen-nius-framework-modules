# ShenNiusFrameworkModules

#### 介绍

这里存储的是所有神牛模块化开发框架的组件Nuget包，不定期更新升级，最终目的只有一个就是以积木式的样子，采取模块化开发，快速交付。

[神牛模块化源码](http://https://gitee.com/shenniu_code_group/godox-modulesshell)
#### 使用说明

<table>
    <thead>
    <tr>
        <th>Package</th>
        <th align="left">NuGet</th>
        <th align="left">Downloads</th>
    </tr>
    </thead>
    <tr>
        <td>ShenNius.Caches</td>
        <td> <img src="https://img.shields.io/nuget/v/ShenNius.Caches?logo=nuget" alt="ShenNius.Caches"></td>
        <td><img src="https://img.shields.io/nuget/dt/ShenNius.Caches.svg?style=flat-square"/></td>
    </tr>    
     <tr>
        <td>ShenNius.FileManagement</td>
        <td> <img src="https://img.shields.io/nuget/v/ShenNius.FileManagement?logo=nuget" alt="ShenNius.Caches"></td>
        <td><img src="https://img.shields.io/nuget/dt/ShenNius.FileManagement.svg?style=flat-square"/></td>
    </tr>    
     <tr>
        <td>ShenNius.ModuleCore</td>
        <td> <img src="https://img.shields.io/nuget/v/ShenNius.ModuleCore?logo=nuget" alt="ShenNius.Caches"></td>
        <td><img src="https://img.shields.io/nuget/dt/ShenNius.ModuleCore.svg?style=flat-square"/></td>
    </tr>
     <tr>
        <td>ShenNius.Repository</td>
        <td> <img src="https://img.shields.io/nuget/v/ShenNius.Repository?logo=nuget" alt="ShenNius.Caches"></td>
        <td><img src="https://img.shields.io/nuget/dt/ShenNius.Repository.svg?style=flat-square"/></td>
    </tr>
</table>

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
