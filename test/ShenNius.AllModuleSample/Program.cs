using ShenNius.FileManagement;
using ShenNius.Repository;
using SqlSugar;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
//builder.Services.AddShenNiusCache(o => { 
//o.Enable=true;
//    o.Database = 1;
//    o.InstanceName = "shennius:";
//    o.Connection = "127.0.0.1:6379,password=123456,connectTimeout=500,connectRetry=1,syncTimeout=500";
//});

builder.Services.AddSqlsugarSetup(option => {
    option.ConnectionString = builder.Configuration["ConnectionStrings:MySql"];
option.DbType = DbType.MySql;
    option.IsAutoCloseConnection = true;
    option.InitKeyType = InitKeyType.Attribute;//从特性读取主键自增信息
});

builder.Services.AddFileUpload(option =>
{
    option.FileType = FileType.QiniuCloud;
    option.QiNiuOss=new QiNiuOss()
    {
        Ak= "mkqrMACRHRttqy5GfFUqw-Jqvm0mBNb7tPeT7YWK",
        Sk= "XMkzGA2hhrCBpfuDUv7BAxRVcXsfdlzTAoKt67nV",
        Bucket= "febfenlg",
        BasePath= "/",
        ImgDomain= "http://imgs.shenniushaha.com/"
    }; 
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
