﻿
using Microsoft.Extensions.DependencyInjection;
using ShenNius.FileManage;
using System;

namespace ShenNius.FileManagement
{
    public static class FileManagemenServiceExtensions
    {
        public static IServiceCollection AddFileUpload(this IServiceCollection services, Action<UploadOption> setupAction)   
        {
            if (setupAction == null)
            {
                throw new ArgumentNullException("setupAction");
            }
            UploadOption option =new UploadOption();    
            setupAction.Invoke(option);
            //是否启用本地文件上传 选择性注入
            if (option.FileType== FileType.Local)
            {
                services.AddSingleton<IUploadFile, LocalFile>();
            }
            if (option.FileType == FileType.QiniuCloud)
            {
                //七牛云配置信息读取
                if (option.QiNiuOss==null)
                {
                    throw new ArgumentNullException(nameof(option.QiNiuOss));
                }
                QiniuCloudIFile qiniuCloudIFile = new QiniuCloudIFile(option.QiNiuOss);
                services.AddSingleton(qiniuCloudIFile);
                services.AddSingleton<IUploadFile>(qiniuCloudIFile);
            }
            
            return services;
        }
    }
}
