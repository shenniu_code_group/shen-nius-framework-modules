﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShenNius.FileManagement
{
    public enum FileType
    {
        /// <summary>
        /// 本地上传
        /// </summary>
        Local,
        /// <summary>
        /// 七牛云
        /// </summary>
        QiniuCloud
    }
}
