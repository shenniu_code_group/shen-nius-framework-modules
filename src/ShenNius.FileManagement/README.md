# ShenNius.FileManagement

#### 介绍
ShenNius.FileManagement是基于Asp.NetCore对文件管理封装的组件，包括常见的上传，删除，列表加载。支持本地上传和七牛云，主要目的是方便模块化开发使用。

#### 软件架构
整体围绕"高内聚，低耦合"的思想，依赖抽象而不依赖于具体。

#### 安装教程

1.  Nuget搜ShenNius.FileManagement选择安装
2.  在startup或program中进行注入
    启用本地上传：    
    services.AddFileUpload(option =>
    {
        option.FileType = FileType.Local;
    });
    启用七牛云上传：    
    services.AddFileUpload(option =>
    {
           option.FileType = FileType.QiniuCloud;
           option.QiNiuOss=new QiNiuOss()
           {
               Ak= "mkqrMACRHRttqy5GfFUqw-Jqvm0mBNb7tPeT7YWK",
               Sk= "XMkzGA2hhrCBpfuDUv7BAxRVcXsfdlzTAoKt67nV",
               Bucket= "febfenlg",
               BasePath= "/",
               ImgDomain= "http://imgs.shenniushaha.com/"
           };
    });
3.  业务构造函数选择IUploadFile进行注入使用，常用方法，也可以查看源码。

#### 使用说明
1.  ShenNius.FileManagement目前只支持Asp.NetCore

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

