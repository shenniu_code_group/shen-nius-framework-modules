# ShenNius.ModuleCore

#### 介绍
ShenNius.ModuleCore是基于ASP.NET Core打造的模块化组件，轻量级，适用于中小项目模块化操作，主要目的是方便业务模块化快速开发使用。

#### 软件架构
整体围绕"高内聚，低耦合"的思想，依赖抽象而不依赖于具体。

#### 安装教程
1. Nuget搜ShenNius.Caches选择安装
2. 新建业务模块类库01 

```
public class 01ApiModule : AppModule
   {
       public override void OnConfigureServices(ServiceConfigurationContext context)
        {
           ...常用业务注入
        }
        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();
            var env = context.ServiceProvider.GetRequiredService<IWebHostEnvironment>();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
        }
   }
```
    

3.  新建业务模块类库02

```
 [DependsOn(
       typeof(01ApiModule)
      )]
    public class 02ApiModule : AppModule
    {
        public override void OnConfigureServices(ServiceConfigurationContext context)
         {
            ...常用业务注入
         }
         public override void OnApplicationInitialization(ApplicationInitializationContext context)
         {
             ...管道操作
         }
    }
```

4.  新建Asp.NetCoreMVC,然后新建一个类，如下：
```
[DependsOn(
         typeof(01ApiModule),
          typeof(02ApiModule)
          )]
    public class ShenNiusMvcAdminModule : AppModule
     {
     }
```

5. 在startup或program中调用：

```
 builder.Services.AddModule<ShenNiusMvcAdminModule>(builder.Configuration);
    app.UseModule();
```

#### 使用说明
1.  ShenNius.ModuleCore目前只支持Asp.NetCore，且引用了下面三个类库：
```
    <PackageReference Include="Microsoft.AspNetCore.Http.Abstractions" Version="2.2.0" />
    <PackageReference Include="Microsoft.Extensions.Configuration.Abstractions" Version="6.0.0" />
    <PackageReference Include="Microsoft.Extensions.DependencyInjection.Abstractions" Version="6.0.0" />
```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

