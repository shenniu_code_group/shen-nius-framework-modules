﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ShenNius.TransferJob
{
    public interface IBackgroundRunService
    {
        Task Execute(CancellationToken cancellationToken);
        void Transfer<T>(Expression<Func<T, Task>> expression);
        void Transfer(Expression<Action> expression);
    }
}
