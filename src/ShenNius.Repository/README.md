# ShenNius.Repository

#### 介绍
ShenNius.Repository是基于sqlsugar的仓储封装，支持多种数据库且友好容器注入，主要目的是方便模块化开发。

#### 软件架构
整体围绕"高内聚，低耦合"的思想，依赖抽象而不依赖于具体。

#### 安装教程

1.  Nuget搜ShenNius.Repository选择安装
2.  在startup或program中进行注入
    services.AddSqlsugarSetup(option =>
     {
         option.ConnectionString = context.Configuration["ConnectionStrings:MySql"];
         option.DbType = DbType.MySql;
         option.IsAutoCloseConnection = true;
         option.InitKeyType = InitKeyType.Attribute;//从特性读取主键自增信息
     });           
3.  如果需要注入仓储泛型，即按下面注入(具体注入的生命周期，请按业务情况使用)。
    services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
4. 一般情况下数据库实体模型结合泛型Repository，基本满足常见的业务。如果有其它业务，可以使用：
     public interface IEntityRepository : IBaseRepository<Entity>
     {
         ...接口
     }
     public class EntityRepository : BaseRepository<Entity>, IEntityRepository
     {
         ...实现业务
     }
5.  别忘记自己新增的接口(4)进行依赖注入就使用。
#### 使用说明

1.  ShenNius.Repository目前只支持Asp.NetCore


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

