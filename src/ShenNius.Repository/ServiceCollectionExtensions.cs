﻿using Microsoft.Extensions.DependencyInjection;
using SqlSugar;
using System;

namespace ShenNius.Repository
{
    public static class ServiceCollectionExtensions 
    {

        public static void AddSqlsugarSetup(this IServiceCollection services, Action<ConnectionConfig> setupAction)
        {
            if (setupAction == null)
            {
                throw new ArgumentNullException(nameof(setupAction));
            }
            ConnectionConfig connectionConfig = new ConnectionConfig();
            setupAction.Invoke(connectionConfig);
            if (connectionConfig==null)
            {
                throw new ArgumentNullException(nameof(connectionConfig));
            }
            SqlSugarScope sqlSugar = new SqlSugarScope(new ConnectionConfig()
            {
                DbType = connectionConfig.DbType,
                ConnectionString = connectionConfig.ConnectionString,
                IsAutoCloseConnection = connectionConfig.IsAutoCloseConnection,
                InitKeyType=connectionConfig.InitKeyType,   
            },
                db =>{
                    //单例参数配置，所有上下文生效  //输出sql
#if DEBUG 
                    db.Aop.OnLogExecuting = (sql, pars) =>
                    {
                        Console.WriteLine(sql);
                    };
                    db.Aop.OnError = (exp) =>//执行SQL 错误事件
                    {
                        string s = exp.Sql;
                    };                   
#endif
                });
            //这边是SqlSugarScope用AddSingleton
            services.AddSingleton<ISqlSugarClient>(sqlSugar);
        }
    }
}
